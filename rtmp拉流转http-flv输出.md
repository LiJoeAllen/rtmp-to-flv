# ***rtmp拉流转http-flv输出***

## **1.** ***环境***

- 服务器系统CentOS8.3

- Web服务器：Nginx和nginx-http-flv-module模块作为推流服务端

- 推流端工具FFmpeg

nginx-http-flv-module与nginx-rtmp-module的功能对比

|       功能       | nginx-http-flv-module | nginx-rtmp-module |                             备注                             |
| :--------------: | :-------------------: | :---------------: | :----------------------------------------------------------: |
| HTTP-FLV (播放)  |           √           |         ×         | nginx-http-flv-module支持HTTPS-FLV支持HTTPS-FLV和chunked回复 |
|     GOP缓存      |           √           |         ×         |                                                              |
|     虚拟主机     |           √           |         ×         |                                                              |
| 省略`listen`配置 |                       |                   |                   配置中必须有一个`listen`                   |
|    纯音频支持    |           √           |                   |            `wait_video`或`wait_key`开启后无法工作            |
| `reuseport`支持  |           √           |         x         | nginx-rtmp-module开启wait_video或者wait_key选项后不能正常工作 |
| 定时打印访问记录 |           √           |         x         |                                                              |
|  JSON风格的stat  |           √           |         x         |                                                              |

## **2.** ***下载***

 还有很多依赖，不安装 会有很多麻烦

```
yum -y install vim* wget unzip gcc-c++ pcre pcre-devel zlib zlib-devel openssl openssl-devel

```

​    [nginx](https://nginx.org/en/download.html)推荐下载Stable version

```
wget http://nginx.org/download/nginx-1.18.0.tar.gz && tar -zxvf nginx-1.18.0.tar.gz
```

 nginx-http-flv-module配合nginx搭建流服务（https://github.com/winshining/nginx-http-flv-module.git）

```
https://github.com/winshining/nginx-http-flv-module.git
```

 下载flv.js用于前端播放flv，也可以使用https://github.com/DIYgod/DPlayer

```
npm install flv.js 
```

## **3.** ***安装***

 将nginx-http-flv-module模板添加到nginx中，生成make文件 并安装nginx

```
./configure --prefix=nginx路径  --add-module=模块路径
```

 编译并安装nginx

```
make && make install
```

 安装ffmpeg (一路Yes)

```
yum install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-8.noarch.rpm
yum install http://rpmfind.net/linux/epel/8/Everything/x86_64/Packages/s/SDL_net-1.2.8-16.el8.x86_64.rpm
yum install ffmpeg
```

 检查(不成功的大概率是少依赖)【具体问题具体分析并具体对待】

```
 rpm -qi ffmpeg
 ffmpeg -version
```

## **4.** ***配置***

NginxConfig示例仅作参考（http服务和rtmp服务相互作用）[使用 nginx -t 指令查看路径和配置正确性]

``` 
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile            on;
    keepalive_timeout  65;
    server {
        listen       80;								      # http-flv的拉流端口
        server_name  localhost;
        location /live {									  # http-flv的相关配置用于前端访问flv流
            flv_live on; 									  # 打开HTTP播放FLV直播流功能
            chunked_transfer_encoding  on;					    # 支持'Transfer-Encoding: chunked'方式回复
            add_header 'Access-Control-Allow-Origin' '*'; 		 # 添加额外的HTTP头
            add_header 'Access-Control-Allow-Credentials' 'true'; # 添加额外的HTTP头
        }
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }
  include /usr/local/nginx/conf/vhost/*.conf;
}

rtmp {  
    out_queue               4096;
    out_cork                 8;
    max_streams             128; 
    timeout                 15s;
    drop_idle_publisher     15s;
    log_interval 5s; 					#log模块在access.log中记录日志的间隔时间，对调试非常有用
    log_size     1m; 					#log模块用来记录日志的缓冲区大小
    server {  
        listen 9999;      				#监听的端口号
        
        #ffmpeg推流的application
        application live {     			#自定义的名字用于ffmpeg推流到nginx
            live on;  
            hls on;  
            hls_path /tmp/live;   		#临时文件存放目录
            hls_fragment 1s;
            hls_playlist_length 3s; 
            gop_cache on; 				#打开GOP缓存，减少首屏等待时间
       }
    } 
}

```

html示例一座参考（使用flv.js实现flv播放）

``` html
<!DOCTYPE html>
<html>
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title>flv.js demo</title>
    <style>
        .mainContainer {
            display: block;
            width: 1024px;
            margin-left: auto;
            margin-right: auto;
        }

        .urlInput {
            display: block;
            width: 100%;
            margin-left: auto;
            margin-right: auto;
            margin-top: 8px;
            margin-bottom: 8px;
        }

        .centeredVideo {
            display: block;
            width: 100%;
            height: 576px;
            margin-left: auto;
            margin-right: auto;
            margin-bottom: auto;
        }

        .controls {
            display: block;
            width: 100%;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
</head>
<body  width="960" height="720">
<div class="mainContainer"  width="960" height="720">
    <video name="videoElement" id="videoElement" class="centeredVideo" controls muted autoplay width="960" height="720">
        你的浏览器太老了，不支持HTML5视频。
    </video>
</div>
<script src="flv.js/dist/flv.js"></script>
<!--导入之前下载的flv.js-->
<script>
    function start() {
        if (flvjs.isSupported()) {
            var videoElement = document.getElementById('videoElement');
            var flvPlayer = flvjs.createPlayer({
                type: 'flv',
                enableWorker: true,     //浏览器端开启flv.js的worker,多进程运行flv.js
                isLive: true,           //直播模式
                hasAudio: true,         //音频             
                hasVideo: true,		    //视频
                stashInitialSize: 128,  
                enableStashBuffer: false, //播放flv时，设置是否启用播放缓存，只在直播起作用。
                url: 'http://192.168.1.110:80/live?port=9999&app=live&stream=cctv'
                //第一个“live”是nginx中http服务配置
                //port是nginx配置rtmp服务的端口
                //app是rtmp服务配置中的application
                //stream是ffmpeg推流的文件名
            });
            flvPlayer.attachMediaElement(videoElement);
            flvPlayer.load();
            flvPlayer.play();
        }
    }

    document.addEventListener('DOMContentLoaded', function () {
        start();
    });
</script>
</body>
</html>


```

## **5.** ***运行***

运行服务前需要配置一下环境变量

``` shell
#打开编辑文件
vim /etc/profile

#添加环境变量
PATH=....在此后面用:分割添加nginx和ffmpeg执行文件目录
export PATH

#执行生效
source /etc/profile
```

启动nginx(配置完环境变量后可直接运行)

```
nginx
```

使用ffmpeg拉流推流

-i 需要转换的rtmp

-q是nginx搭建的rtmp服务地址 live对应nginx配置中的rtmp-server-application cctv对应前端显示转流后的临时文件名

``` shell
ffmpeg  
-i "rtmp://58.200.131.2:1935/livetv/cctv1" 
-f flv 
-vcodec libx264 
-vprofile baseline 
-acodec aac 
-f flv  
-q 10  rtmp://192.168.1.110:9999/live/cctv
```

## **6.** ***完结***

此后ip加http端口号可以展示rtmp转flv后的视频

