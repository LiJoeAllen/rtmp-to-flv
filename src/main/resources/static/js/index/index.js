window.flvUrl = "cctv2";

function start() {
    if (flvjs.isSupported()) {
        const videoElement = document.getElementById('videoElement');
        const flvPlayer = flvjs.createPlayer({
            type: 'flv',
            enableWorker: true,     //浏览器端开启flv.js的worker,多进程运行flv.js
            isLive: true,           //直播模式
            hasAudio: true,         //音频
            hasVideo: true,		    //视频
            stashInitialSize: 128,
            enableStashBuffer: false, //播放flv时，设置是否启用播放缓存，只在直播起作用。
            url: 'http://192.168.1.110:80/live?port=9999&app=live&stream=' + window.flvUrl
            //第一个“live”是nginx中http服务配置
            //port是nginx配置rtmp服务的端口
            //app是rtmp服务配置中的application
            //stream是ffmpeg推流的文件名
        });
        flvPlayer.attachMediaElement(videoElement);
        flvPlayer.load();
        flvPlayer.play();
    }
}

document.addEventListener('DOMContentLoaded', function () {
    start();
});


function loading() {
    var layer = layui.layer;
    var index = layer.load(1);
    const rtmp = document.getElementById("rtmp").value;
    layui.jquery.ajax({
        type: "post",
        url: "rtmp",
        dataType: "json",
        async: true,
        data: {
            "rtmp": rtmp
        },
        success: function (data) {
            console.log("data:" + data);
            if (data === "1") {
                layer.msg('加载失败');
            } else {
                if (data !== window.flvUrl) {
                    window.flvUrl = data
                }

                start();
            }
            layer.close(index);
        },
        error: function () {
            layer.msg("错误");
            layer.close(index);
        }
    });
}

layui.jquery("#rtmp").keydown(function (e) {
    if (e.keyCode === 13) {
        loading();
    }
});