package top.joeallen.rtmptoflv.service;

import java.util.Random;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import top.joeallen.rtmptoflv.dao.RtmpToFlvMapper;
import top.joeallen.rtmptoflv.entity.RtmpToFlvEntity;

/**
 * @author JoeAllen_Li
 * @Date 2021/3/22 13:57
 * @describe 类描述
 */
@Service
public class RtmpToFlvService implements RtmpToFlvMapper {
    @Resource
    RtmpToFlvMapper rtmpToFlvMapper;

    @Override
    public int insert(RtmpToFlvEntity record) {
        return 0;
    }

    @Override
    public int insertSelective(RtmpToFlvEntity record) {
        return 0;
    }

    @Override
    public int update(RtmpToFlvEntity record) {
        return 0;
    }

    @Override
    public int updateSelective(RtmpToFlvEntity record) {
        return 0;
    }

    @Override
    public RtmpToFlvEntity queryOne(Integer id) {
        return rtmpToFlvMapper.queryOne(id);
    }

    @Override
    public RtmpToFlvEntity queryAll(String cloud) {
        RtmpToFlvEntity rtmpToFlvEntity = rtmpToFlvMapper.queryAll(cloud);
        if (rtmpToFlvEntity == null) {
            RtmpToFlvEntity insertSelectiveEntity = new RtmpToFlvEntity(null, String.valueOf(System.currentTimeMillis()) + new Random().nextInt(999), cloud);
            if (rtmpToFlvMapper.insertSelective(insertSelectiveEntity) == 1) {
                rtmpToFlvEntity = rtmpToFlvMapper.queryAll(cloud);
            }
        }
        return rtmpToFlvEntity;
    }

    @Override
    public int delete(Integer id) {
        return 0;
    }
}
