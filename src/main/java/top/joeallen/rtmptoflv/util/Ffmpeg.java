package top.joeallen.rtmptoflv.util;

import java.io.IOException;
import java.util.Arrays;

import org.bytedeco.javacpp.Loader;

/**
 * @author JoeAllen_Li
 * @Date 2021/4/8 11:06
 * @describe 类描述
 */
public class Ffmpeg {
    public static void run(String... args) {
        try {
            String ffmpeg = Loader.load(org.bytedeco.ffmpeg.ffmpeg.class);
//            String ffmpeg = "D:\\tools\\.gradle\\caches\\modules-2\\files-2.1\\org.bytedeco.javacpp-presets\\ffmpeg\\4.1-1.4.4\\9c06ae199b2a32ff8ce36d22273f432739c8ff08\\";
            String[] command = new String[args.length + 1];
            command[0] = ffmpeg;
            System.arraycopy(args, 0, command, 1, args.length);
            System.out.println(Arrays.toString(command));
            ProcessBuilder pb = new ProcessBuilder(command);
            pb.inheritIO().start().waitFor();
        } catch (InterruptedException |
                IOException e) {
//            e.printStackTrace();
        }
    }
}
