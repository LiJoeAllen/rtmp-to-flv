package top.joeallen.rtmptoflv.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * @author JoeAllen_Li
 */
public class CommandUtil {
    /**
     * 在指定路径下执行一条命令，不能执行cd之类的命令
     *
     * @param command 要执行的Linux命令
     * @param dir     目标路径，在该路径执行上述Linux命令
     * @return 命令执行后显示的结果
     * @throws IOException
     */
    public static String run(String command, File dir) throws IOException {
        Scanner input = null;
        // 加上命令本身
        StringBuilder result = new StringBuilder(command + "\n");
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(command, null, dir);
            try {
                //等待命令执行完成
                process.waitFor(10, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            InputStream is = process.getInputStream();
            input = new Scanner(is);
            while (input.hasNextLine()) {
                result.append(input.nextLine()).append("\n");
            }
        } finally {
            if (input != null) {
                input.close();
            }
            if (process != null) {
                process.destroy();
            }
        }
        return result.toString();
    }

    /**
     * 在指定路径下，开启脚本，可以执行多条命令
     *
     * @param command 要执行的Linux命令
     * @param dir     目标路径，在该路径执行上述Linux命令
     * @return 所有命令执行后显示的结果
     * @throws IOException
     */
    public static List<String> run(String[] command, File dir) throws IOException {
        BufferedReader in = null;
        PrintWriter out = null;
        List<String> resultList = new ArrayList<>();
        Process process = null;
        try {
            // 开启脚本是为了能够获取所有的返回结果
            // /bin/sh开启shell脚本
            process = Runtime.getRuntime().exec("/bin/sh", null, dir);
            in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(process.getOutputStream())), true);
            for (String cmd : command) {
                // 一条条执行命令
                out.println(cmd);
            }
            // 表示脚本结束，必须执行，否则in流不结束。
            out.println("exit");

            try {
                // 等待命令执行完成
                process.waitFor(10, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // 装填返回结果，比如ls命令执行后显示的一行行字符串
            String line = "";
            while ((line = in.readLine()) != null) {
                resultList.add(line);
            }
            // 加上命令本身，可根据需要自行取舍
            resultList.add(0, Arrays.toString(command));
        } finally {
            if (out != null) {
                out.close();
            }
            if (in != null) {
                in.close();
            }
            if (process != null) {
                process.destroy();
            }
        }
        return resultList;
    }

}