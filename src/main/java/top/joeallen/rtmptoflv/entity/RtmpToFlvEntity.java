package top.joeallen.rtmptoflv.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author MybatisHelperPro
 * @date 2021/03/22
 * @see <a href="https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro">MybatisHelperPro</a>
 */
public class RtmpToFlvEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String to;

    private String cloud;

    public RtmpToFlvEntity() {
    }

    public RtmpToFlvEntity(Integer id, String to, String cloud) {
        this.id = id;
        this.to = to;
        this.cloud = cloud;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCloud() {
        return cloud;
    }

    public void setCloud(String cloud) {
        this.cloud = cloud;
    }

    @Override
    public String toString() {
        return "TestEntity{" +
                "id=" + id +
                ", to='" + to + '\'' +
                ", cloud='" + cloud + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RtmpToFlvEntity o1 = (RtmpToFlvEntity) o;
        return Objects.equals(id, o1.id) &&
                Objects.equals(to, o1.to) &&
                Objects.equals(cloud, o1.cloud);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, to, cloud);
    }
}