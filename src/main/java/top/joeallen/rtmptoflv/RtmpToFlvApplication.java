package top.joeallen.rtmptoflv;


import java.net.InetAddress;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.context.WebServerApplicationContext;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.boot.web.server.WebServer;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @author JoeAllen_Li
 */

@Slf4j
@EnableOpenApi
@SpringBootApplication
@MapperScan(basePackages = {"top.joeallen.rtmptoflv.dao"})
public class RtmpToFlvApplication implements ApplicationListener<WebServerInitializedEvent> {
//    private final static Logger logger = LoggerFactory.getLogger(RtmpToFlvApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(RtmpToFlvApplication.class, args);
//        Ffmpeg.run("-1","dfasdf");
    }

    @SneakyThrows
    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        WebServer server = event.getWebServer();
        WebServerApplicationContext context = event.getApplicationContext();
        Environment env = context.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        int port = server.getPort();
        String contextPath = env.getProperty("server.servlet.context-path");
        if (contextPath == null) {
            contextPath = "";
        }
        log.info("\n---------------------------------------------------------\n" +
                "\t应用程序正在运行!访问地址:\n" +
                "\t本地:\thttp://localhost:{}\n" +
                "\t外部:\thttp://{}:{}{}" +
                "\n---------------------------------------------------------\n", port, ip, port, contextPath);
    }
}
