package top.joeallen.rtmptoflv.dao;

import org.springframework.stereotype.Repository;

import top.joeallen.rtmptoflv.entity.RtmpToFlvEntity;

/**
 * @author MybatisHelperPro
 * @date 2021/03/18
 * @see <a href="https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro">MybatisHelperPro</a>
 */
@Repository
public interface RtmpToFlvMapper {

    int insert(RtmpToFlvEntity record);

    int insertSelective(RtmpToFlvEntity record);

    int update(RtmpToFlvEntity record);

    int updateSelective(RtmpToFlvEntity record);

    RtmpToFlvEntity queryOne(Integer id);

    RtmpToFlvEntity queryAll(String cloud);

    int delete(Integer id);
}