package top.joeallen.rtmptoflv.conf;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author JoeAllen_Li
 */
@Configuration
public class SwaggerConfig {
    /**
     * swagger的配置文件，这里可以配置swagger的一些基本的内容，比如扫描的包等等
     *
     * @return z
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.OAS_30).apiInfo(apiInfo())
                // 是否开启
                .enable(true).select()
                // 扫描的路径包
                .apis(RequestHandlerSelectors.basePackage("top.joeallen.rtmptoflv"))
                // 指定路径处理PathSelectors.any()代表所有的路径
                .paths(PathSelectors.any()).build().pathMapping("/");
    }

    /**
     * 构建 api文档的详细信息函数,注意这里的注解引用的是哪个
     *
     * @return z
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                // 页面标题
                .title("RTMP转FLV")
                // 创建人信息
                .contact(new Contact("JoeAllen", "https://JoeAllen.top", "2494136784@qq.com"))
                // 版本号
                .version("1.0")
                // 描述
                .description("随便玩玩").build();
    }
}