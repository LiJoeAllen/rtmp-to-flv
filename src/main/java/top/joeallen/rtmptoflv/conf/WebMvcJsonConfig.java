package top.joeallen.rtmptoflv.conf;

import java.lang.reflect.Type;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import springfox.documentation.spring.web.json.Json;

/**
 * @author JoeAllen_Li
 */
@Configuration
public class WebMvcJsonConfig {
    @Bean
    GsonHttpMessageConverter gsonHttpMessageConverter() {
        GsonHttpMessageConverter converter = new GsonHttpMessageConverter();
        converter.setGson(
                new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").serializeNulls()
                        .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory<>())
                        .registerTypeAdapter(Json.class, new WebMvcJsonConfig.SpringfoxJsonToGsonAdapter())
                        .create());
        return converter;
    }

    private static class SpringfoxJsonToGsonAdapter implements JsonSerializer<Json> {


        @Override
        public JsonElement serialize(Json src, Type typeOfSrc, JsonSerializationContext context) {
            final JsonParser parser = new JsonParser();
            return parser.parse(src.value());
        }
    }

}
