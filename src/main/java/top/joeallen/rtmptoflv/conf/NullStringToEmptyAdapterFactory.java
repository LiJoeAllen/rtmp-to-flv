package top.joeallen.rtmptoflv.conf;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;

/**
 * @author JoeAllen_Li
 */
public class NullStringToEmptyAdapterFactory<T> implements TypeAdapterFactory {
    @SuppressWarnings("unchecked")
    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        TypeAdapter<T> result = null;
        Class<T> rawType;
        rawType = (Class<T>) type.getRawType();
        if (rawType == String.class) {
            result = (TypeAdapter<T>) new StringNullAdapter();
        }
        return result;
    }
}